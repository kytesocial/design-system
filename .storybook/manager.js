import { addons } from '@storybook/addons'
import oneTheme from './theme'

addons.setConfig({
  isFullscreen: false,
  showNav: true,
  showPanel: true,
  panelPosition: 'bottom',
  sidebarAnimations: true,
  enableShortcuts: true,
  isToolshown: true,
  theme: oneTheme,
  selectedPanel: undefined,
  initialActive: 'sidebar',
  showRoots: false
})
