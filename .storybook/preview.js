import React from 'react';
import { MINIMAL_VIEWPORTS } from '@storybook/addon-viewport'

import '../src/styles/index.scss';
import './styles.scss';

const customViewports = {
  kindleFire2: {
    name: 'Kindle Fire 2',
    styles: {
      width: '600px',
      height: '963px'
    }
  },
  kindleFireHD: {
    name: 'Kindle Fire HD',
    styles: {
      width: '533px',
      height: '801px'
    }
  }
}

const scssReq = require.context('!!raw-loader!../src/styles/tokens/', true, /.\.scss$/)
const scssTokenFiles = scssReq
  .keys()
  .map(filename => ({ filename, content: scssReq(filename).default }))

export const decorators = [
  (Story) => (
    <>
      <Story />
    </>
  )
];

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  // Storybook a11y addon configuration
  a11y: {
    // the target DOM element
    element: '#root',
    // sets the execution mode for the addon
    manual: false,
  },
  options: {
    storySort: {
      method: 'alphabetical',
      order: ['Getting Started', 'Components']
    }
  },
  viewport: {
    viewports: {
      ...MINIMAL_VIEWPORTS,
      ...customViewports
    }
  },
  controls: {
    expanded: true
  },
  layout: 'padding',
  designToken: {
    files: {
      scss: scssTokenFiles
    }
  }
}
