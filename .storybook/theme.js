import { create } from '@storybook/theming/create'
import logoSvg from '../src/assets/one-logo.svg'

export default create({
  base: 'light',

  //   colorPrimary: 'hotpink',
  //   colorSecondary: 'deepskyblue',

  // UI
  //   appBg: 'white',
  //   appContentBg: 'silver',
  //   appBorderColor: 'grey',
  //   appBorderRadius: 4,

  // Typography
  //   fontBase: '"Open Sans", sans-serif',
  //   fontCode: 'monospace',

  // Text colors
  //   textColor: 'black',
  //   textInverseColor: 'rgba(255,255,255,0.9)',

  // Toolbar default and active colors
  //   barTextColor: 'silver',
  //   barSelectedColor: 'black',
  //   barBg: 'hotpink',

  // Form colors
  //   inputBg: 'white',
  //   inputBorder: 'silver',
  //   inputTextColor: 'black',
  //   inputBorderRadius: 4,

  brandTitle: 'One Company Closer Design System',
  brandUrl: 'https://blueninja.io/',
  brandImage: logoSvg,
  gridCellSize: 12
})