# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.0.0-alpha.4](https://bitbucket.org/kytesocial/design-system/compare/v1.0.0-alpha.3...v1.0.0-alpha.4) (2020-12-04)


### Features

* **storybook/preview-head:** add quicksand font ([b9440ac](https://bitbucket.org/kytesocial/design-system/commit/b9440ac8266f5d454eacd233f4603d2b3f50ae89))


### Bug Fixes

* **token:** remove quicksand font google import ([7057649](https://bitbucket.org/kytesocial/design-system/commit/7057649b757e67f674744d59d2f16e3b5845625a))

## [1.0.0-alpha.3](https://bitbucket.org/kytesocial/design-system/compare/v1.0.0-alpha.2...v1.0.0-alpha.3) (2020-11-14)


### Features

* **storybook/theme:** new one logo svg ([38e550b](https://bitbucket.org/kytesocial/design-system/commit/38e550b1e9b6b53b1c3a117619a76b87d42f60cc))
* **styles:** Quicksand token font as Base ([99e6988](https://bitbucket.org/kytesocial/design-system/commit/99e6988e2e8222c405b1a3e0778f7c7e9dadf6e5))
* **system:** add OneCardColumns ([d7065bd](https://bitbucket.org/kytesocial/design-system/commit/d7065bd6974ce34a61e9d7919f9be48b892d16f1))
* **tokens:** add flag ([e736ebb](https://bitbucket.org/kytesocial/design-system/commit/e736ebb1e33dc0487f2e5ad0e66da5628db9525a))

## [1.0.0-alpha.2](https://bitbucket.org/kytesocial/design-system/compare/v1.0.0-alpha.1...v1.0.0-alpha.2) (2020-11-13)


### Features

* **system:** import Container, Row, Col ([af50be4](https://bitbucket.org/kytesocial/design-system/commit/af50be42177b90ce25874deb9a90f01e323a7e59))

## 1.0.0-alpha.1 (2020-11-10)


### Features

* **assets:** occ-logo.svg ([a51430a](https://bitbucket.org/kytesocial/design-system/commit/a51430aa6f08c2424a9c0327218d2300f5f77f8e))
* **avatar:** rename to AvatarOcc ([22d1631](https://bitbucket.org/kytesocial/design-system/commit/22d16313429a0272205098c882d9806d887c4085))
* **badge:** add bootstrap figma design ([263da78](https://bitbucket.org/kytesocial/design-system/commit/263da78a046e6d99e43cf05d34c40b5595112dc6))
* **badge:** mdx story with bootstrap badge ([e3aef5e](https://bitbucket.org/kytesocial/design-system/commit/e3aef5ebb13ebe763761455c17a2355dc7bc18b3))
* **badge:** new story based on system ([efb0ce9](https://bitbucket.org/kytesocial/design-system/commit/efb0ce92bf26c9e5eede3bd9f66b7db656264f2e))
* **badge:** variant story ([b8670c7](https://bitbucket.org/kytesocial/design-system/commit/b8670c7995b32dff8947d826b7d2cac38faac238))
* **button:** add bootstrap figma design ([0cb9613](https://bitbucket.org/kytesocial/design-system/commit/0cb9613ecc045011120c49d0d34da97c38669793))
* **button:** config codePreview parameter ([3178129](https://bitbucket.org/kytesocial/design-system/commit/3178129fc1cfed0758000764b33b2147c9855a4a))
* **button:** variant story ([a6aec6f](https://bitbucket.org/kytesocial/design-system/commit/a6aec6fca7ba8d6036809e1fc3531a4b2a6e0ac3))
* **Button:** story ([4c52455](https://bitbucket.org/kytesocial/design-system/commit/4c52455d9a4874a08e804b53915ceb4ff2c9a291))
* **componenets:** remove Button & Link ([b4320ec](https://bitbucket.org/kytesocial/design-system/commit/b4320ec3457cc7d1c8c925c54080a84feb3e5e24))
* **Icon:** rename to OccIcon ([70764b2](https://bitbucket.org/kytesocial/design-system/commit/70764b254eb1f0399adc749cec579db72944ed73))
* **styles:** add tokens & move colors innit ([c2e41aa](https://bitbucket.org/kytesocial/design-system/commit/c2e41aad791e6b2bbb446d8119e8dc6edd902d7a))
* **styles:** update bootstrap colors & variants ([d59d0fe](https://bitbucket.org/kytesocial/design-system/commit/d59d0fefb1785af4092955af926565d942047b4c))
* **variant:** add dark-2 ([7a92431](https://bitbucket.org/kytesocial/design-system/commit/7a924310a66c69d9f9c0968f3613a64aef20be18))
* add Bootstrap scss ([2061068](https://bitbucket.org/kytesocial/design-system/commit/20610680335151349f1b4395a7e4c0bef4d80714))


### Bug Fixes

* **storybook:** background-preview styles ([9b2eb0d](https://bitbucket.org/kytesocial/design-system/commit/9b2eb0dee3380b5c6b88c24df3423e8979fa5245))

# v0.1.0 (Tue Sep 03 2019)

- Created first version of the design system, with `Avatar`, `Badge`, `Button`, `Icon` and `Link` components.

#### Authors: 1

- Tom Coleman ([@tmeasday](https://github.com/tmeasday))