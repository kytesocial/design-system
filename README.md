# ONE Design System
 
This Design System is build for all One Company Closer (ONE) related projects like One, OFC, OWC and etc.

## Scripts 

### Project Setup

`npm install`

### Compiles and hot-reloads Storybook docs for development

`npm run storybook`

### Compiles and minifies Storybook for production

`npm run build-storybook`
 
### Compiles and minifies Storybook docs for production

`npm run build-storybook-docs`

### Compiles and minifies System for distrubtion

`npm run build`

- refer to [Distribute UI across an organization](https://www.learnstorybook.com/design-systems-for-developers/react/en/distribute/)

## Release Tagging Steps

- refer to [standard-version docs](https://github.com/conventional-changelog/standard-version)

1. Checkout to `main` branch by running `git checkout main`
2. Ensure you have the latest commit in the branch bt running `git pull`
3. Run `npm run build` to update the system dist files
4. Make sure all commits follow [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/) standards
5. Run `npm run release`, there will be 4 steps to automate the versioning:
- define [semantic version specification](https://www.conventionalcommits.org/en/v1.0.0/#how-does-this-relate-to-semver)
- generate [conventional-changelog](https://github.com/conventional-changelog/conventional-changelog-config-spec/blob/master/versions/2.1.0/README.md)
- generate a new commit for updating package.json, npm run-lock.json & changelog.md
- create a new git tag

> NOTE: if you need manually define the next release version, add `--release-as` flag, e.g:
`npm run release -- --release-as minor`

6. Review generated commit and tag
7. Once everything is okay push it to the repo:

`git push --follow-tags origin main`

8. Go to the Bitbucket repo and edit the docs under the latest generated tag. You can easily update the copy-paste generated content from `CHANGELOG.md`.
9. Deploy the latest code to your staging environemnt (if any).
10. Success!

## Use in different project

1. Install DS into your `package.json` file:

`git+https://Aidosmf@bitbucket.org/kytesocial/design-system.git#v1.0.0-alpha.1` 

2. Import a react component:

```
import { OneButton } from '@one/design-system';
```

```jsx
/* Render the component on your page */
<OneButton variant="primary">test</OneButton>
```

3. [Font Loading](https://github.com/storybookjs/design-system#font-loading)
4. Install any SASS loader to be able to read and compile DS SCSS files.
5. Import SASS styles:

```scss
// under your main SCSS file
@import '@one/design-system/dist/styles/index.scss';
```

- If you need to modify or control SASS files, you can affect them by importing each SASS file separately, you can access them under `@one/design-system/styles/` folder. For more understanding please follow [Bootstrap Theming](https://getbootstrap.com/docs/4.5/getting-started/theming/)